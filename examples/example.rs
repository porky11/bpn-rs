use bpn::{LoadError, SafeNet, SaveError, SimulatedNet};

use std::{
    collections::HashMap,
    fs::File,
    io::{self, BufRead, BufReader, Error as IOError},
    str::FromStr,
};

#[derive(Debug)]
enum ExampleError {
    IO(IOError),
    Save(SaveError),
    Load(LoadError),
}

impl From<IOError> for ExampleError {
    fn from(err: IOError) -> Self {
        Self::IO(err)
    }
}

impl From<SaveError> for ExampleError {
    fn from(err: SaveError) -> Self {
        Self::Save(err)
    }
}

impl From<LoadError> for ExampleError {
    fn from(err: LoadError) -> Self {
        Self::Load(err)
    }
}

fn main() -> Result<(), ExampleError> {
    let mut net = SimulatedNet::load("examples/example.pns".as_ref())?;

    let mut names = HashMap::new();
    let file = File::open("examples/example.pnk")?;
    for (tid, line) in net.transition_ids().zip(BufReader::new(file).lines()) {
        names.insert(tid, line?);
    }

    #[cfg(feature = "save")]
    net.save("example_copy.pns".as_ref())?;

    #[cfg(not(feature = "load"))]
    net.add_state();
    #[cfg(feature = "load")]
    net.load_state("examples/example.pnsl".as_ref())
        .expect("State initialization failed");
    let mut forward = true;
    for mut state in &mut net {
        loop {
            let fire = if forward {
                state.fire()
            } else {
                state.unfire()
            };

            if fire.transitions.is_empty() {
                println!("Reverse play direction!");
                forward = !forward;
                continue;
            }

            for (i, tid) in fire.transitions.iter().enumerate() {
                println!("{}: {}", i + 1, names[tid]);
            }
            let stdin = io::stdin();
            let mut string = String::new();
            let size = stdin.read_line(&mut string)?;
            if size == 0 {
                continue;
            }
            match string.chars().next().unwrap() {
                'r' => {
                    println!("Reverse play direction!");
                    forward = !forward;
                    continue;
                }
                'e' => {
                    break;
                }
                's' => {
                    println!(
                        "{}",
                        if state.save("examples/example.pnsl".as_ref()).is_ok() {
                            "Saving successful"
                        } else {
                            "Saving failed"
                        }
                    );
                    continue;
                }
                _ => (),
            }
            match usize::from_str(&string[..(string.len() - 1)]) {
                Ok(num) if num != 0 && num <= fire.transitions.len() => {
                    fire.finish(num - 1);
                }
                _ => {
                    println!(
                        "You have to input a valid number from 1 to {}",
                        fire.transitions.len()
                    );
                    println!("You can also quit by writing \"q\", save the current state by writing \"s\" or reverse by writing \"r\"");
                    continue;
                }
            }
        }
    }

    Ok(())
}
