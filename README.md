# Better petri nets for rust

This is a rust binding to my [binary petri net simulator written in C](https://gitlab.com/porky11/bpn).
The rust binding tries to be safer than the C API, and simplifies interactive editing and reaction to changes.
The example should do exactly the same as in the C version.

For more information look at the [documentation](https://docs.rs/bpn/) or have a look at the [C API](https://gitlab.com/porky11/bpn).

Also have a look at the [petri net editor](https://docs.rs/pn-editor/), which makes use of the whole API, and helps you understand, how it works.
