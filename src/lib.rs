#![deny(missing_docs)]
/*!
See the [description](https://crates.io/crates/bpn)
**/

mod sys;

pub use self::sys::types::{Net, Node, State};

use self::sys::functions::*;

use std::{
    fs::{self, File},
    io::{Error as IOError, Read, Write},
    path::Path,
};

fn read_values(filename: &Path) -> Result<Vec<u32>, IOError> {
    let size = (fs::metadata(filename)?.len() + 3) / 4;

    let mut file = File::open(filename)?;

    (0..size)
        .map(|_| {
            let mut value = [0; 4];
            file.read_exact(&mut value)?;
            Ok(u32::from_le_bytes(value))
        })
        .collect()
}

/// The error type when saving fails.
#[derive(Debug)]
pub enum SaveError {
    /// An IO error.
    IO(IOError),
}

impl From<IOError> for SaveError {
    fn from(err: IOError) -> Self {
        Self::IO(err)
    }
}

/// The error type when loading fails.
#[derive(Debug)]
pub enum LoadError {
    /// An IO error.
    IO(IOError),
    /// Loaded the invalid format.
    InvalidFormat,
}

impl From<IOError> for LoadError {
    fn from(err: IOError) -> Self {
        Self::IO(err)
    }
}

/// The identifier meant to represent transitions.
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Tid(usize);

impl Tid {
    #[inline(always)]
    fn assert(self, count: usize) {
        assert!(self.0 < count, "Transition id out of range");
    }
}

/// The identifier meant to represent places.
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Pid(usize);

impl Pid {
    #[inline(always)]
    fn assert(self, count: usize) {
        assert!(self.0 < count, "Place id out of range");
    }
}

/// A wrapper type for net to support managing multiple states for simulations.
pub struct SimulatedNet {
    net: Net,
    /// The states of the net. Cannot be changed directly.
    pub states: Vec<State>,
}

impl Clone for SimulatedNet {
    fn clone(&self) -> Self {
        let net = self.net.clone();
        let mut states = Vec::new();
        for old_state in &self.states {
            states.push(unsafe {
                let mut state = std::mem::MaybeUninit::uninit();
                bpnCloneState(state.as_mut_ptr(), old_state, &net);
                state.assume_init()
            })
        }
        Self { net, states }
    }
}

impl std::ops::Deref for SimulatedNet {
    type Target = Net;
    fn deref(&self) -> &Net {
        &self.net
    }
}

/// An iterator to iterate over the states of a simulated net.
pub struct Iter<'net> {
    net: &'net Net,
    states: std::slice::Iter<'net, State>,
}

/// A mutable iterator to iterate over the states of a simulated net.
pub struct IterMut<'net> {
    net: &'net Net,
    states: std::slice::IterMut<'net, State>,
}

/// A simulation state combined with the net it belongs to.
pub struct SimulationState<'net> {
    net: &'net Net,
    /// The real state value.
    pub state: &'net State,
}

/// A simulation state combined with the net it belongs to, ready for simualtion.
pub struct SimulationStateMut<'net> {
    net: &'net Net,
    /// The real state value.
    pub state: &'net mut State,
}

impl<'net> IntoIterator for &'net SimulatedNet {
    type Item = SimulationState<'net>;
    type IntoIter = Iter<'net>;

    fn into_iter(self) -> Self::IntoIter {
        Iter {
            net: &self.net,
            states: (&self.states).iter(),
        }
    }
}

impl<'net> IntoIterator for &'net mut SimulatedNet {
    type Item = SimulationStateMut<'net>;
    type IntoIter = IterMut<'net>;

    fn into_iter(self) -> Self::IntoIter {
        IterMut {
            net: &self.net,
            states: (&mut self.states).iter_mut(),
        }
    }
}

impl<'net> Iterator for Iter<'net> {
    type Item = SimulationState<'net>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(state) = self.states.next() {
            Some(SimulationState {
                net: &self.net,
                state,
            })
        } else {
            None
        }
    }
}

impl<'net> Iterator for IterMut<'net> {
    type Item = SimulationStateMut<'net>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(state) = self.states.next() {
            Some(SimulationStateMut {
                net: &self.net,
                state,
            })
        } else {
            None
        }
    }
}

/// A dynamic type, able to store both net kinds.
pub enum DynamicNet {
    /// Variant for the default `Net` type.
    Default(Net),
    /// Variant for the `SimulatedNet` type.
    Simulated(SimulatedNet),
}

/// An iterator over some ids.
pub struct Ids<Id> {
    id: Id,
    max: Id,
}

impl Iterator for Ids<Pid> {
    type Item = Pid;

    fn next(&mut self) -> Option<Pid> {
        if self.id.0 < self.max.0 {
            let id = self.id.0;
            self.id.0 += 1;
            Some(Pid(id))
        } else {
            None
        }
    }
}

impl DoubleEndedIterator for Ids<Pid> {
    fn next_back(&mut self) -> Option<Pid> {
        if self.id.0 < self.max.0 {
            self.max.0 -= 1;
            Some(Pid(self.max.0))
        } else {
            None
        }
    }
}

impl ExactSizeIterator for Ids<Pid> {
    fn len(&self) -> usize {
        self.max.0 - self.id.0
    }
}

impl Iterator for Ids<Tid> {
    type Item = Tid;

    fn next(&mut self) -> Option<Tid> {
        if self.id.0 < self.max.0 {
            let id = self.id.0;
            self.id.0 += 1;
            Some(Tid(id))
        } else {
            None
        }
    }
}

impl DoubleEndedIterator for Ids<Tid> {
    fn next_back(&mut self) -> Option<Tid> {
        if self.id.0 < self.max.0 {
            self.max.0 -= 1;
            Some(Tid(self.max.0))
        } else {
            None
        }
    }
}

impl ExactSizeIterator for Ids<Tid> {
    fn len(&self) -> usize {
        self.max.0 - self.id.0
    }
}

/// A trait to share safe functionality for the default `Net` type and the `SimulatedNet`
pub trait SafeNet {
    /// Save the petri net to a file. Result represents success.
    fn save(&self, filename: &Path) -> Result<(), SaveError>;
    /// The count of transitions of the petri net.
    fn transition_count(&self) -> usize;
    /// An iterator over the ids of existing transitions.
    fn transition_ids(&self) -> Ids<Tid>;
    /// A node representing a transition of the petri net.
    fn transition(&self, tid: Tid) -> &Node<Pid>;
    /// Returns the index of the next reusable transition.
    fn reusable_transition(&self) -> Option<Tid>;
    /// The count of places of the petri net.
    fn place_count(&self) -> usize;
    /// An iterator over the ids of existing places.
    fn place_ids(&self) -> Ids<Pid>;
    /// A node representing a place of the petri net.
    fn place(&self, pid: Pid) -> &Node<Tid>;
    /// Returns the index of the next reusable place.
    fn reusable_place(&self) -> Option<Pid>;
    /// The initial token count for a place of the petri net.
    fn initial_activated(&self, pid: Pid) -> bool;

    /// Add a new place to the petri net and get the index.
    fn add_place(&mut self) -> Pid;

    /// Add a new transition to the petri net and get the index.
    fn add_transition(&mut self) -> Tid;

    /// Add a new transition to the petri net, connct it to the specified places and get the index.
    fn add_connected_transition(&mut self, pids: &[Pid]) -> Tid;

    /// Remove a place at index `pid` from petri net.
    fn remove_place(&mut self, pid: Pid);

    /// Make a connection out from the transitoin with index `tid` to place with index `pid`.
    /// Result represents success.
    fn connect_out(&mut self, tid: Tid, pid: Pid) -> bool;

    /// Make a connection into transition with index `tid` from place with index `pid`.
    fn disconnect_in(&mut self, tid: Tid, pid: Pid);

    /// Duplicate the transition and get the index of the clone.
    fn duplicate_transition(&mut self, tid: Tid) -> Tid;

    /// Duplicate the place and get the index of the clone.
    fn duplicate_place(&mut self, pid: Pid) -> Pid;

    /// Increase the initial token count in place indexed by `pid`.
    fn activate(&mut self, pid: Pid);

    /// Creates a dynamic net type from a net.
    fn dynamic(self) -> DynamicNet;
}

impl DynamicNet {
    /// Convert the dynamic net into a `Net`
    pub fn default(&self) -> Option<&Net> {
        if let DynamicNet::Default(net) = self {
            Some(net)
        } else {
            None
        }
    }
    /// Convert the dynamic net into a mutable `Net`
    pub fn default_mut(&mut self) -> Option<&mut Net> {
        if let DynamicNet::Default(net) = self {
            Some(net)
        } else {
            None
        }
    }
    /// Convert the dynamic net into a `SimulatedNet`
    pub fn simulated(&self) -> Option<&SimulatedNet> {
        if let DynamicNet::Simulated(net) = self {
            Some(net)
        } else {
            None
        }
    }
    /// Convert the dynamic net into a mutable `SimulatedNet`
    pub fn simulated_mut(&mut self) -> Option<&mut SimulatedNet> {
        if let DynamicNet::Simulated(net) = self {
            Some(net)
        } else {
            None
        }
    }
    /// Convert the dynamic net into a `SafeNet`
    pub fn safe(&self) -> &dyn SafeNet {
        match self {
            DynamicNet::Default(net) => net,
            DynamicNet::Simulated(net) => net,
        }
    }
    /// Convert the dynamic net into a mutable `SafeNet`
    pub fn safe_mut(&mut self) -> &mut dyn SafeNet {
        match self {
            DynamicNet::Default(net) => net,
            DynamicNet::Simulated(net) => net,
        }
    }
}

impl SimulatedNet {
    /// Create a new, empty petri net in simulated state.
    pub fn new() -> SimulatedNet {
        SimulatedNet {
            net: Net::new(),
            states: Vec::new(),
        }
    }
    /// Load a petri net from a file in simulated state.
    pub fn load(filename: &Path) -> Result<SimulatedNet, LoadError> {
        Ok(SimulatedNet {
            net: Net::load(filename)?,
            states: Vec::new(),
        })
    }
    /// Convert a `Net` to a `SimulatedNet` for advanced simulation.
    pub fn from_net(net: Net) -> SimulatedNet {
        SimulatedNet {
            net,
            states: Vec::new(),
        }
    }

    /// Add a new, empty state for simulation of a petri net.
    /// The state is bound to the lifetime of the simulated net.
    pub fn add_state(&mut self) {
        let state = State::new(&self.net);
        self.states.push(state);
    }

    /// Load a simulation state from a file.
    /// Using the wrong file layout will fail or create confusing state.
    pub fn load_state(&mut self, filename: &Path) -> Result<(), LoadError> {
        let state = State::load(&self.net, filename)?;
        self.states.push(state);
        Ok(())
    }

    /// Get the simulation state of a net at the specified index.
    pub fn state<'net>(&'net self, index: usize) -> SimulationState<'net> {
        SimulationState {
            net: &self.net,
            state: &self.states[index],
        }
    }

    /// Get the simulation state of a net at the specified index.
    pub fn state_mut<'net>(&'net mut self, index: usize) -> SimulationStateMut<'net> {
        SimulationStateMut {
            net: &self.net,
            state: &mut self.states[index],
        }
    }

    /// Calculate the number of states.
    pub fn len(&self) -> usize {
        self.states.len()
    }

    /// Release the `Net` from the `SimulatedNet` again.
    /// Destroys the simulation state.
    pub fn release(self) -> Net {
        self.net
    }

    // ensure all states are updated.
    fn update_states(&mut self) {
        for state in &mut self.states {
            unsafe {
                bpnState_refresh(state, &self.net);
            }
        }
    }
}

impl SafeNet for SimulatedNet {
    #[inline]
    fn save(&self, filename: &Path) -> Result<(), SaveError> {
        self.net.save(filename)
    }

    #[inline]
    fn transition_count(&self) -> usize {
        self.net.transition_count()
    }
    #[inline]
    fn transition_ids(&self) -> Ids<Tid> {
        self.net.transition_ids()
    }
    #[inline]
    fn transition(&self, tid: Tid) -> &Node<Pid> {
        self.net.transition(tid)
    }
    #[inline]
    fn reusable_transition(&self) -> Option<Tid> {
        self.net.reusable_transition()
    }
    #[inline]
    fn place_count(&self) -> usize {
        self.net.place_count()
    }
    #[inline]
    fn place_ids(&self) -> Ids<Pid> {
        self.net.place_ids()
    }
    #[inline]
    fn place(&self, pid: Pid) -> &Node<Tid> {
        self.net.place(pid)
    }
    #[inline]
    fn reusable_place(&self) -> Option<Pid> {
        self.net.reusable_place()
    }
    #[inline]
    fn initial_activated(&self, pid: Pid) -> bool {
        self.net.initial_activated(pid)
    }

    #[inline]
    fn add_place(&mut self) -> Pid {
        let result = self.net.add_place();
        self.update_states();
        result
    }
    #[inline]
    fn add_transition(&mut self) -> Tid {
        let result = self.net.add_transition();
        self.update_states();
        result
    }
    #[inline]
    fn add_connected_transition(&mut self, pids: &[Pid]) -> Tid {
        let result = self.net.add_connected_transition(pids);
        self.update_states();
        result
    }

    #[inline]
    fn remove_place(&mut self, pid: Pid) {
        let result = self.net.remove_place(pid);
        self.update_states();
        result
    }

    #[inline]
    fn connect_out(&mut self, tid: Tid, pid: Pid) -> bool {
        let result = self.net.connect_out(tid, pid);
        self.update_states();
        result
    }
    #[inline]
    fn disconnect_in(&mut self, tid: Tid, pid: Pid) {
        let result = self.net.disconnect_in(tid, pid);
        self.update_states();
        result
    }

    #[inline]
    fn duplicate_transition(&mut self, tid: Tid) -> Tid {
        let result = self.net.duplicate_transition(tid);
        self.update_states();
        result
    }
    #[inline]
    fn duplicate_place(&mut self, pid: Pid) -> Pid {
        let result = self.net.duplicate_place(pid);
        self.update_states();
        result
    }
    #[inline]
    fn activate(&mut self, pid: Pid) {
        self.net.activate(pid);
        self.update_states();
    }

    fn dynamic(self) -> DynamicNet {
        DynamicNet::Simulated(self)
    }
}

impl<Id> Node<Id> {
    /// Get a slice of indices to the next nodes.
    pub fn next(&self) -> &[Id] {
        unsafe { std::slice::from_raw_parts(self.next, self.next_count) }
    }

    /// Get a slice of indices to the previous nodes.
    pub fn prev(&self) -> &[Id] {
        unsafe { std::slice::from_raw_parts(self.prev, self.prev_count) }
    }
}

impl Net {
    /// Create a new, empty petri net.
    pub fn new() -> Net {
        unsafe {
            let mut net = std::mem::MaybeUninit::uninit();
            bpnCreateNet(net.as_mut_ptr());
            net.assume_init()
        }
    }
    /// Load a petri net from a file.
    pub fn load(filename: &Path) -> Result<Net, LoadError> {
        let values = read_values(filename)?;

        unsafe {
            let mut net = std::mem::MaybeUninit::uninit();

            if bpnLoadNet(net.as_mut_ptr(), values.len(), &values[0]) {
                Ok(net.assume_init())
            } else {
                Err(LoadError::InvalidFormat)
            }
        }
    }
    /// Remove a transition at index `tid` from petri net.
    pub fn remove_transition(&mut self, tid: Tid) {
        tid.assert(self.transition_count);
        unsafe { bpnNet_removeTransition_unsafe(self, tid.0) }
    }
    /// Make a connection into transition with index `tid` from place with index `pid`.
    /// Result represents success.
    pub fn connect_in(&mut self, tid: Tid, pid: Pid) -> bool {
        tid.assert(self.transition_count);
        pid.assert(self.place_count);
        unsafe { bpnNet_connectIn_unsafe(self, tid.0, pid.0) }
    }
    /// Remove the connection out from transition with index `tid` to place with index `pid`.
    pub fn disconnect_out(&mut self, tid: Tid, pid: Pid) {
        tid.assert(self.transition_count);
        pid.assert(self.place_count);
        unsafe { bpnNet_disconnectOut_unsafe(self, tid.0, pid.0) }
    }
}

impl SafeNet for Net {
    fn save(&self, filename: &Path) -> Result<(), SaveError> {
        let mut file = File::create(filename)?;

        let data = unsafe {
            let count = bpnNet_serializeSize(self);
            let mut data = vec![0u32; count];
            bpnNet_serialize(self, &mut data[0]);
            data
        };

        for value in data {
            file.write_all(&value.to_le_bytes())?
        }

        Ok(())
    }

    fn transition_count(&self) -> usize {
        self.transition_count
    }
    fn transition_ids(&self) -> Ids<Tid> {
        Ids {
            id: Tid(0),
            max: Tid(self.transition_count),
        }
    }
    fn transition(&self, tid: Tid) -> &Node<Pid> {
        unsafe {
            &std::slice::from_raw_parts(self.transitions as *const Node<_>, self.transition_count)
                [tid.0]
        }
    }
    fn reusable_transition(&self) -> Option<Tid> {
        if let Some(list) = unsafe { self.reusable_transitions.as_ref() } {
            Some(Tid(list.index))
        } else {
            None
        }
    }
    fn place_count(&self) -> usize {
        self.place_count
    }
    fn place_ids(&self) -> Ids<Pid> {
        Ids {
            id: Pid(0),
            max: Pid(self.place_count),
        }
    }
    fn place(&self, pid: Pid) -> &Node<Tid> {
        unsafe {
            &std::slice::from_raw_parts(self.places as *const Node<_>, self.place_count)[pid.0]
        }
    }
    fn reusable_place(&self) -> Option<Pid> {
        if let Some(list) = unsafe { self.reusable_places.as_ref() } {
            Some(Pid(list.index))
        } else {
            None
        }
    }
    fn initial_activated(&self, pid: Pid) -> bool {
        unsafe { std::slice::from_raw_parts(self.initial_activated, self.place_count)[pid.0] }
    }

    fn add_place(&mut self) -> Pid {
        unsafe { Pid(bpnNet_addPlace(self)) }
    }
    fn add_transition(&mut self) -> Tid {
        unsafe { Tid(bpnNet_addTransition(self)) }
    }
    fn add_connected_transition(&mut self, pids: &[Pid]) -> Tid {
        for pid in pids {
            pid.assert(self.place_count);
        }
        Tid(unsafe {
            bpnNet_addConnectedTransition(self, pids.len(), pids.as_ptr() as *const usize)
        })
    }

    fn remove_place(&mut self, pid: Pid) {
        pid.assert(self.place_count);
        unsafe { bpnNet_removePlace(self, pid.0) }
    }

    fn connect_out(&mut self, tid: Tid, pid: Pid) -> bool {
        tid.assert(self.transition_count);
        pid.assert(self.place_count);
        unsafe { bpnNet_connectOut(self, tid.0, pid.0) }
    }
    fn disconnect_in(&mut self, tid: Tid, pid: Pid) {
        tid.assert(self.transition_count);
        pid.assert(self.place_count);
        unsafe { bpnNet_disconnectIn(self, tid.0, pid.0) }
    }

    fn duplicate_transition(&mut self, tid: Tid) -> Tid {
        tid.assert(self.transition_count);
        Tid(unsafe { bpnNet_duplicateTransition(self, tid.0) })
    }
    fn duplicate_place(&mut self, pid: Pid) -> Pid {
        pid.assert(self.place_count);
        Pid(unsafe { bpnNet_duplicatePlace(self, pid.0) })
    }
    fn activate(&mut self, pid: Pid) {
        pid.assert(self.place_count);
        unsafe { bpnNet_activate(self, pid.0) }
    }

    fn dynamic(self) -> DynamicNet {
        DynamicNet::Default(self)
    }
}

impl Clone for Net {
    fn clone(&self) -> Self {
        unsafe {
            let mut net = std::mem::MaybeUninit::uninit();
            bpnCloneNet(net.as_mut_ptr(), self);
            net.assume_init()
        }
    }
}

impl Drop for Net {
    fn drop(&mut self) {
        unsafe {
            bpnDestroyNet(self);
        }
    }
}

impl State {
    fn new(net: &Net) -> State {
        unsafe {
            let mut state = std::mem::MaybeUninit::uninit();
            bpnCreateState(state.as_mut_ptr(), net);
            state.assume_init()
        }
    }

    fn load(net: &Net, filename: &Path) -> Result<State, LoadError> {
        let values = read_values(filename)?;

        unsafe {
            let mut state = std::mem::MaybeUninit::uninit();
            if bpnLoadState(state.as_mut_ptr(), net, values.len(), &values[0]) {
                Ok(state.assume_init())
            } else {
                Err(LoadError::InvalidFormat)
            }
        }
    }

    fn changed_transitions(
        &mut self,
        call: unsafe extern "C" fn(state: *mut State, *mut usize, *mut usize),
    ) -> Box<[Tid]> {
        unsafe {
            let mut count = std::mem::MaybeUninit::uninit();
            call(self, count.as_mut_ptr(), std::ptr::null_mut());
            let mut count = count.assume_init();
            let mut transitions: Vec<Tid> = Vec::with_capacity(count);
            transitions.set_len(count);
            call(self, &mut count, transitions.as_mut_ptr() as *mut usize);
            transitions.into_boxed_slice()
        }
    }

    /// Get a list of newly fireable transitions since the last call of this method.
    /// Automatically clears that list.
    pub fn added_transitions(&mut self) -> Box<[Tid]> {
        self.changed_transitions(bpnState_addedTransitions)
    }

    /// Get a list of newly fireable transitions when playing backwards since the last call of this method.
    /// Automatically clears that list.
    pub fn added_transitions_backwards(&mut self) -> Box<[Tid]> {
        self.changed_transitions(bpnState_addedTransitions_backwards)
    }

    /// Get a list of nomore fireable transitions since the last call of this method.
    /// Automatically clears that list.
    pub fn removed_transitions(&mut self) -> Box<[Tid]> {
        self.changed_transitions(bpnState_removedTransitions)
    }

    /// Get a list of nomore fireable transitions when playing backwards since the last call of this method.
    /// Automatically clears that list.
    pub fn removed_transitions_backwards(&mut self) -> Box<[Tid]> {
        self.changed_transitions(bpnState_removedTransitions_backwards)
    }
}

impl Drop for State {
    fn drop(&mut self) {
        unsafe {
            bpnDestroyState(self);
        }
    }
}

impl<'net> SimulationState<'net> {
    /// Save the current state of the simulation.
    pub fn save(&self, filename: &Path) -> Result<(), SaveError> {
        let mut file = File::create(filename)?;
        for tid in self.net.transition_ids() {
            file.write_all(&[self.call_state(tid).into()])?;
        }

        Ok(())
    }

    /// Get the current call state of the specified transition.
    /// Mostly important for loading
    pub fn call_state(&self, tid: Tid) -> bool {
        unsafe {
            std::slice::from_raw_parts(self.state.call_states, self.net.transition_count)[tid.0]
        }
    }

    /// Get the current activation state of the specified place.
    pub fn activated(&self, pid: Pid) -> bool {
        unsafe { std::slice::from_raw_parts(self.state.activated, self.net.place_count)[pid.0] }
    }
}

impl<'net> SimulationStateMut<'net> {
    /// Save the current state of the simulation.
    pub fn save(&self, filename: &Path) -> Result<(), SaveError> {
        let mut file = File::create(filename)?;
        for &state in self.call_states() {
            file.write_all(&[state.into()])?;
        }

        Ok(())
    }

    fn call_states(&self) -> &[bool] {
        unsafe { std::slice::from_raw_parts(self.state.call_states, self.net.transition_count) }
    }

    /// Lists fireable transitions without being able to fire them.
    pub fn fireable(&mut self) -> Box<[Tid]> {
        unsafe {
            let mut count = std::mem::MaybeUninit::uninit();
            bpnState_transitions(self.state, count.as_mut_ptr(), std::ptr::null_mut());
            let mut count = count.assume_init();
            let mut transitions: Vec<Tid> = Vec::with_capacity(count);
            transitions.set_len(count);
            let mut transitions = transitions.into_boxed_slice();
            bpnState_transitions(
                self.state,
                &mut count,
                transitions.as_mut_ptr() as *mut usize,
            );
            transitions
        }
    }

    /// Lists unfireable transitions without being able to unfire them.
    pub fn unfireable(&mut self) -> Box<[Tid]> {
        unsafe {
            let mut count = std::mem::MaybeUninit::uninit();
            bpnState_transitions_backwards(self.state, count.as_mut_ptr(), std::ptr::null_mut());
            let mut count = count.assume_init();
            let mut transitions: Vec<Tid> = Vec::with_capacity(count);
            transitions.set_len(count);
            let mut transitions = transitions.into_boxed_slice();
            bpnState_transitions_backwards(
                self.state,
                &mut count,
                transitions.as_mut_ptr() as *mut usize,
            );
            transitions
        }
    }

    /// Generate a `FireState` to fire a transition later.
    pub fn fire<'state>(&'state mut self) -> FireState<'net, 'state> {
        FireState::new(self)
    }

    /// Generate a `FireState` to unfire a transition later.
    pub fn unfire<'state>(&'state mut self) -> FireState<'net, 'state> {
        FireState::new_backwards(self)
    }

    /// Just fire the transition `tid`.
    /// Caution: Be sure to fire only, when you know a transition to be fireable.
    /// Can also be used to unfire the transition when it's known to be unfireable.
    pub unsafe fn fire_unchecked(&mut self, tid: Tid) {
        bpnState_fire(self.state, self.net, tid.0);
    }
}

/// A temporary object for selecting a transition to fire.
pub struct FireState<'net, 'state> {
    state: &'state mut SimulationStateMut<'net>,
    /// A slice of indices of the current available transitions.
    pub transitions: Box<[Tid]>,
}

impl<'net, 'state> FireState<'net, 'state> {
    fn new(state: &'state mut SimulationStateMut<'net>) -> Self {
        FireState {
            transitions: state.fireable(),
            state,
        }
    }

    fn new_backwards(state: &'state mut SimulationStateMut<'net>) -> Self {
        FireState {
            transitions: state.unfireable(),
            state,
        }
    }

    /// Finish the fire by specifying the index of the transition index inside the slice of available indices.
    /// Not using the index of the transition directly may seem weird, but this way it's easy and efficient to ensure, no invalid transition will be called.
    /// And nomrally you know the index anyway and you save the work to get the index manually.
    /// As a result you get back your original state.
    pub fn finish(self, id: usize) {
        unsafe {
            bpnState_fire(self.state.state, self.state.net, self.transitions[id].0);
        }
    }
}
