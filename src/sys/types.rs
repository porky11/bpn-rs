/// A node of a petri net; stores the indices to the next and previous nodes.
#[derive(Debug)]
#[repr(C)]
pub struct Node<Id> {
    /// The count of following nodes of a node.c
    pub next_count: usize,
    next_size: usize,
    pub(crate) next: *mut Id,
    /// The count of preceding nodes of a node.
    pub prev_count: usize,
    prev_size: usize,
    pub(crate) prev: *mut Id,
}

#[repr(C)]
pub struct IndexList {
    pub(crate) index: usize,
    next: *mut IndexList,
}

/// A type representing a petri net. It only stores the initial state, not the state used for simulatoin.
#[derive(Debug)]
#[repr(C)]
pub struct Net {
    pub(crate) transition_count: usize,
    transitions_size: usize,
    pub(crate) transitions: *mut Node<usize>,
    pub(crate) reusable_transitions: *mut IndexList,
    pub(crate) place_count: usize,
    places_size: usize,
    pub(crate) places: *mut Node<usize>,
    pub(crate) initial_activated: *mut bool,
    pub(crate) reusable_places: *mut IndexList,
    dirt: *mut IndexList,
    reverse_dirt: *mut IndexList,
}

#[repr(C)]
struct FireChanges {
    count: usize,
    added_count: usize,
    removed_count: usize,
    active: *mut IndexList,
    added: *mut IndexList,
    removed: *mut IndexList,
}

/// The state of the petri net used for simulation.
#[repr(C)]
pub struct State {
    places_size: usize,
    pub(crate) activated: *mut bool,
    transitions_size: usize,
    pub(crate) call_states: *mut bool,
    fire: FireChanges,
    unfire: FireChanges,
}
