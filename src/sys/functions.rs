use super::types::*;
extern "C" {
    pub(crate) fn bpnCreateNet(net: *mut Net);
    pub(crate) fn bpnCloneNet(net_clone: *mut Net, net: *const Net);
    pub(crate) fn bpnLoadNet(net: *mut Net, count: usize, values: *const u32) -> bool;
    pub(crate) fn bpnDestroyNet(net: *mut Net);
    pub(crate) fn bpnNet_serializeSize(net: *const Net) -> usize;
    pub(crate) fn bpnNet_serialize(net: *const Net, values: *mut u32);

    pub(crate) fn bpnNet_addPlace(net: *mut Net) -> usize;
    pub(crate) fn bpnNet_addTransition(net: *mut Net) -> usize;
    pub(crate) fn bpnNet_addConnectedTransition(
        net: *mut Net,
        place_count: usize,
        place: *const usize,
    ) -> usize;

    pub(crate) fn bpnNet_removePlace(net: *mut Net, pid: usize);
    pub(crate) fn bpnNet_removeTransition_unsafe(net: *mut Net, tid: usize);

    pub(crate) fn bpnNet_connectIn_unsafe(net: *mut Net, tid: usize, pid: usize) -> bool;
    pub(crate) fn bpnNet_connectOut(net: *mut Net, tid: usize, pid: usize) -> bool;
    pub(crate) fn bpnNet_disconnectIn(net: *mut Net, tid: usize, pid: usize);
    pub(crate) fn bpnNet_disconnectOut_unsafe(net: *mut Net, tid: usize, pid: usize);

    pub(crate) fn bpnNet_duplicateTransition(net: *mut Net, tid: usize) -> usize;
    pub(crate) fn bpnNet_duplicatePlace(net: *mut Net, pid: usize) -> usize;
    pub(crate) fn bpnNet_activate(net: *mut Net, pid: usize);

    pub(crate) fn bpnCreateState(state: *mut State, net: *const Net);
    pub(crate) fn bpnCloneState(state_clone: *mut State, state: *const State, net: *const Net);
    pub(crate) fn bpnLoadState(
        state: *mut State,
        net: *const Net,
        count: usize,
        values: *const u32,
    ) -> bool;
    pub(crate) fn bpnDestroyState(state: *mut State);

    pub(crate) fn bpnState_transitions(
        state: *mut State,
        count: *mut usize,
        transitions: *mut usize,
    );
    pub(crate) fn bpnState_transitions_backwards(
        state: *mut State,
        count: *mut usize,
        transitions: *mut usize,
    );
    pub(crate) fn bpnState_addedTransitions(
        state: *mut State,
        count: *mut usize,
        transitions: *mut usize,
    );
    pub(crate) fn bpnState_addedTransitions_backwards(
        state: *mut State,
        count: *mut usize,
        transitions: *mut usize,
    );
    pub(crate) fn bpnState_removedTransitions(
        state: *mut State,
        count: *mut usize,
        transitions: *mut usize,
    );
    pub(crate) fn bpnState_removedTransitions_backwards(
        state: *mut State,
        count: *mut usize,
        transitions: *mut usize,
    );
    #[allow(dead_code)]
    pub(crate) fn bpnState_cleanChanges(state: *mut State);
    #[allow(dead_code)]
    pub(crate) fn bpnState_cleanChanges_backwards(state: *mut State);

    pub(crate) fn bpnState_fire(state: *mut State, net: *const Net, tid: usize);

    pub(crate) fn bpnState_refresh(state: *mut State, net: *const Net);
}
