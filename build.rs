#[cfg(feature = "static-build")]
fn build_static_library() {
    use std::env;
    let target = env::var("TARGET").unwrap();
    let mut build = cc::Build::new();
    build.file("bpn/bpn.c");

    if !target.contains("darwin") && !target.contains("windows") {
        build.flag("-fvisibility=hidden");
    }

    build.compile("bpn");
    println!("cargo:root={}", env::var("OUT_DIR").unwrap());
}

fn main() {
    #[cfg(feature = "static-build")]
    build_static_library();
    #[cfg(not(feature = "static-build"))]
    println!("cargo:rustc-link-lib=dylib=bpn");
}
